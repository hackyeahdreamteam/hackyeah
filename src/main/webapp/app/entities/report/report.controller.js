(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('ReportController', ReportController);

    ReportController.$inject = ['Report', 'LengthCoords'];

    function ReportController(Report, LengthCoords) {

        var vm = this;

        vm.reports = [];
        vm.getDistance = LengthCoords.getDistance;
        loadAll();
        
        function loadAll() {
            Report.query(function(result) {
                vm.reports = result;
                vm.searchQuery = null;
            });
        }
    }
})();
