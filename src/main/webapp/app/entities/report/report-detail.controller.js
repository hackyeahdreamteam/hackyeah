(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('ReportDetailController', ReportDetailController);

    ReportDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Report', 'User', 'ReportCategory', 'Area', 'Severity'];

    function ReportDetailController($scope, $rootScope, $stateParams, previousState, entity, Report, User, ReportCategory, Area, Severity) {
        var vm = this;

        vm.report = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hackYeahApp:reportUpdate', function(event, result) {
            vm.report = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
