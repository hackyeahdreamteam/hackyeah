(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('UserSettingsController', UserSettingsController);

    UserSettingsController.$inject = ['UserSettings'];

    function UserSettingsController(UserSettings) {

        var vm = this;

        vm.userSettings = [];

        loadAll();

        function loadAll() {
            UserSettings.query(function(result) {
                vm.userSettings = result;
                vm.searchQuery = null;
            });
        }
    }
})();
