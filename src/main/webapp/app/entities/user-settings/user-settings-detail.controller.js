(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('UserSettingsDetailController', UserSettingsDetailController);

    UserSettingsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserSettings', 'User', 'Location'];

    function UserSettingsDetailController($scope, $rootScope, $stateParams, previousState, entity, UserSettings, User, Location) {
        var vm = this;

        vm.userSettings = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hackYeahApp:userSettingsUpdate', function(event, result) {
            vm.userSettings = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
