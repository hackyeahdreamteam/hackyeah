(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('LocationDetailController', LocationDetailController);

    LocationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Location', 'UserSettings', 'Report'];

    function LocationDetailController($scope, $rootScope, $stateParams, previousState, entity, Location, UserSettings, Report) {
        var vm = this;

        vm.location = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hackYeahApp:locationUpdate', function(event, result) {
            vm.location = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
