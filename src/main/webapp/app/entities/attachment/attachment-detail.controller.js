(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('AttachmentDetailController', AttachmentDetailController);

    AttachmentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Attachment', 'Report'];

    function AttachmentDetailController($scope, $rootScope, $stateParams, previousState, entity, Attachment, Report) {
        var vm = this;

        vm.attachment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hackYeahApp:attachmentUpdate', function(event, result) {
            vm.attachment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
