(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('SeverityDialogController', SeverityDialogController);

    SeverityDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Severity', 'Report'];

    function SeverityDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Severity, Report) {
        var vm = this;

        vm.severity = entity;
        vm.clear = clear;
        vm.save = save;
        vm.reports = Report.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.severity.id !== null) {
                Severity.update(vm.severity, onSaveSuccess, onSaveError);
            } else {
                Severity.save(vm.severity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('hackYeahApp:severityUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
