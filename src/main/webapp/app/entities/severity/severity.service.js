(function() {
    'use strict';
    angular
        .module('hackYeahApp')
        .factory('Severity', Severity);

    Severity.$inject = ['$resource'];

    function Severity ($resource) {
        var resourceUrl =  'api/severities/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
