(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('severity', {
            parent: 'entity',
            url: '/severity',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'hackYeahApp.severity.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/severity/severities.html',
                    controller: 'SeverityController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('severity');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('severity-detail', {
            parent: 'severity',
            url: '/severity/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'hackYeahApp.severity.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/severity/severity-detail.html',
                    controller: 'SeverityDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('severity');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Severity', function($stateParams, Severity) {
                    return Severity.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'severity',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('severity-detail.edit', {
            parent: 'severity-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/severity/severity-dialog.html',
                    controller: 'SeverityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Severity', function(Severity) {
                            return Severity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('severity.new', {
            parent: 'severity',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/severity/severity-dialog.html',
                    controller: 'SeverityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                value: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('severity', null, { reload: 'severity' });
                }, function() {
                    $state.go('severity');
                });
            }]
        })
        .state('severity.edit', {
            parent: 'severity',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/severity/severity-dialog.html',
                    controller: 'SeverityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Severity', function(Severity) {
                            return Severity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('severity', null, { reload: 'severity' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('severity.delete', {
            parent: 'severity',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/severity/severity-delete-dialog.html',
                    controller: 'SeverityDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Severity', function(Severity) {
                            return Severity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('severity', null, { reload: 'severity' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
