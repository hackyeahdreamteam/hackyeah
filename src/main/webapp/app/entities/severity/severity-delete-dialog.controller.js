(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('SeverityDeleteController',SeverityDeleteController);

    SeverityDeleteController.$inject = ['$uibModalInstance', 'entity', 'Severity'];

    function SeverityDeleteController($uibModalInstance, entity, Severity) {
        var vm = this;

        vm.severity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Severity.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
