(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('SeverityDetailController', SeverityDetailController);

    SeverityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Severity', 'Report'];

    function SeverityDetailController($scope, $rootScope, $stateParams, previousState, entity, Severity, Report) {
        var vm = this;

        vm.severity = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hackYeahApp:severityUpdate', function(event, result) {
            vm.severity = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
