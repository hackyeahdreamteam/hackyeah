(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('SeverityController', SeverityController);

    SeverityController.$inject = ['Severity'];

    function SeverityController(Severity) {

        var vm = this;

        vm.severities = [];

        loadAll();

        function loadAll() {
            Severity.query(function(result) {
                vm.severities = result;
                vm.searchQuery = null;
            });
        }
    }
})();
