(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('AreaDetailController', AreaDetailController);

    AreaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Area', 'Report'];

    function AreaDetailController($scope, $rootScope, $stateParams, previousState, entity, Area, Report) {
        var vm = this;

        vm.area = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hackYeahApp:areaUpdate', function(event, result) {
            vm.area = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
