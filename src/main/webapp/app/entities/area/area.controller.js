(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('AreaController', AreaController);

    AreaController.$inject = ['Area'];

    function AreaController(Area) {

        var vm = this;

        vm.areas = [];

        loadAll();

        function loadAll() {
            Area.query(function(result) {
                vm.areas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
