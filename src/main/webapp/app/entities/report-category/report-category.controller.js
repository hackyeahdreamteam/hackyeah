(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('ReportCategoryController', ReportCategoryController);

    ReportCategoryController.$inject = ['ReportCategory'];

    function ReportCategoryController(ReportCategory) {

        var vm = this;

        vm.reportCategories = [];

        loadAll();

        function loadAll() {
            ReportCategory.query(function(result) {
                vm.reportCategories = result;
                vm.searchQuery = null;
            });
        }
    }
})();
