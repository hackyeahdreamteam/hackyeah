(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('ReportCategoryDeleteController',ReportCategoryDeleteController);

    ReportCategoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'ReportCategory'];

    function ReportCategoryDeleteController($uibModalInstance, entity, ReportCategory) {
        var vm = this;

        vm.reportCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ReportCategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
