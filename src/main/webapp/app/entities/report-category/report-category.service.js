(function() {
    'use strict';
    angular
        .module('hackYeahApp')
        .factory('ReportCategory', ReportCategory);

    ReportCategory.$inject = ['$resource'];

    function ReportCategory ($resource) {
        var resourceUrl =  'api/report-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
