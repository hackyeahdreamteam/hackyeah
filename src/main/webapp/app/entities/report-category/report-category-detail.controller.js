(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('ReportCategoryDetailController', ReportCategoryDetailController);

    ReportCategoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ReportCategory', 'Report'];

    function ReportCategoryDetailController($scope, $rootScope, $stateParams, previousState, entity, ReportCategory, Report) {
        var vm = this;

        vm.reportCategory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hackYeahApp:reportCategoryUpdate', function(event, result) {
            vm.reportCategory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
