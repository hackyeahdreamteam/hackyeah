(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('ReportCategoryDialogController', ReportCategoryDialogController);

    ReportCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ReportCategory', 'Report'];

    function ReportCategoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ReportCategory, Report) {
        var vm = this;

        vm.reportCategory = entity;
        vm.clear = clear;
        vm.save = save;
        vm.reports = Report.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.reportCategory.id !== null) {
                ReportCategory.update(vm.reportCategory, onSaveSuccess, onSaveError);
            } else {
                ReportCategory.save(vm.reportCategory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('hackYeahApp:reportCategoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
