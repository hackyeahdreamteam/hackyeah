(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('report-category', {
            parent: 'entity',
            url: '/report-category',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'hackYeahApp.reportCategory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/report-category/report-categories.html',
                    controller: 'ReportCategoryController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reportCategory');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('report-category-detail', {
            parent: 'report-category',
            url: '/report-category/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'hackYeahApp.reportCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/report-category/report-category-detail.html',
                    controller: 'ReportCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reportCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ReportCategory', function($stateParams, ReportCategory) {
                    return ReportCategory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'report-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('report-category-detail.edit', {
            parent: 'report-category-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-category/report-category-dialog.html',
                    controller: 'ReportCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ReportCategory', function(ReportCategory) {
                            return ReportCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('report-category.new', {
            parent: 'report-category',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-category/report-category-dialog.html',
                    controller: 'ReportCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('report-category', null, { reload: 'report-category' });
                }, function() {
                    $state.go('report-category');
                });
            }]
        })
        .state('report-category.edit', {
            parent: 'report-category',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-category/report-category-dialog.html',
                    controller: 'ReportCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ReportCategory', function(ReportCategory) {
                            return ReportCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('report-category', null, { reload: 'report-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('report-category.delete', {
            parent: 'report-category',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-category/report-category-delete-dialog.html',
                    controller: 'ReportCategoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ReportCategory', function(ReportCategory) {
                            return ReportCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('report-category', null, { reload: 'report-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
