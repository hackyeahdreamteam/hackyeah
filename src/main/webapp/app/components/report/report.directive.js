/* globals $ */
(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .directive('myReport', reportfn);

    function reportfn () {
        var directive = {
            replace: true,
            restrict: 'E',
            templateUrl: 'app/components/report/my-report.html',
            scope: {
                content:'=',
            },
            link:linkFunc
        };

        return directive;

        function linkFunc(scope, iElement) {
            scope.getSeverityClass = function(value){
                switch (value){
                    case '1': 
                    return 'severity-green';
                    break;
                    case '2':
                    return 'severity-orange';        
                    break;
                    case '3':   
                    return 'severity-red';
                    break;
                }

            }
        };

    }
})();