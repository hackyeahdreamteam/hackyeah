/* globals $ */
(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .directive('mapPreview', mapPreview);

    function mapPreview () {
        var directive = {
            replace: true,
            restrict: 'E',
            templateUrl: 'app/components/maps/map.directive.html',
            scope: {
            	center: '=',
            	markers: '=',
            	areas: '='
            },
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, iElement) {
        	
        };
    }
})();