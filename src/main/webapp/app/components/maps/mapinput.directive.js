/* globals $ */
(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .directive('mapInput', mapInput);

    function mapInput () {
        var directive = {
            replace: true,
            restrict: 'E',
            templateUrl: 'app/components/maps/mapinput.directive.html',
            scope: {
            	model: '='
            },
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, iElement) {
        	
        	scope.currentPosition = {
        			lat: 55,
        			lng: 55
        	}
        	
        	scope.marker = null;
        	
        	scope.changeEvent = function(lat, lng){
        		model.lat = lat;
        		model.lng = lng;
        	}
        	
        	scope.updateModel = function(){
        		if(scope.model == null){
        			scope.model = {};
        		}
        		scope.model.lat = scope.marker.lat;
        		scope.model.lng = scope.marker.lng;
        	}
        	
        	scope.updateMarkerPosition = function(event){
        		if(scope.marker == null)
    			{
        			scope.marker = {};
    			}
        		scope.marker.lat = event.latLng.lat();
        		scope.marker.lng = event.latLng.lng();
        		scope.updateModel();
        	}
        	
        };
    }
})();