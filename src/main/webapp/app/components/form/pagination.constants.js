(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
