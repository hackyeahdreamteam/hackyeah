(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state'];

    function HomeController ($scope, Principal, LoginService, $state) {
    	$scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE";
        var vm = this;
        $scope.reports = [{
            title:'report-title',
            severity:'1',
            content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget nisl odio. Nulla facilisi. Donec suscipit finibus porta. Donec vulputate tellus enim, ut varius purus.',
            distance: '4'
          },{
              title:'report-title',
              severity:'2',
              content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget nisl odio. Nulla facilisi. Donec suscipit finibus porta. Donec vulputate tellus enim, ut varius purus.',
              distance: '4'
            },{
              title:'report-title',
              severity:'2',
              content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget nisl odio. Nulla facilisi. Donec suscipit finibus porta. Donec vulputate tellus enim, ut varius purus.',
              distance: '4'
            },{
              title:'report-title',
              severity:'3',
              content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget nisl odio. Nulla facilisi. Donec suscipit finibus porta. Donec vulputate tellus enim, ut varius purus.',
              distance: '4'
            },{
              title:'report-title',
              severity:'1',
              content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget nisl odio. Nulla facilisi. Donec suscipit finibus porta. Donec vulputate tellus enim, ut varius purus.',
              distance: '4'
            }
      ];
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });
        
        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }
    }
})();
