(function() {
    'use strict';

    angular
        .module('hackYeahApp')
        .factory('LengthCoords', LengthCoords);

    LengthCoords.$inject = ['$http', 'UserSettings', '$geolocation'];

    function LengthCoords($http, UserSettings, $geolocation) {
    	var Location = UserSettings.current().location;
        
        $geolocation.getCurrentPosition({
            timeout: 60000
         }).then(function(position) {
        	 Location = position;
        	 
         });
        function degreesToRadians(degrees) {
        	  return degrees * Math.PI / 180;
        }

        function distanceInKmBetweenEarthCoordinates(coord2) {
        	  var earthRadiusKm = 6371;
        	  coord1 = Position;
        	  var lat1 = coord1.tatitude;
        	  var lon1 = coord1.longitude;
        	  
        	  var lat2 = coord2.latitude;
        	  var lon2 = coord2.longitude;
        	  
        	  var dLat = degreesToRadians(lat2-lat1);
        	  var dLon = degreesToRadians(lon2-lon1);

        	  lat1 = degreesToRadians(lat1);
        	  lat2 = degreesToRadians(lat2);

        	  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        	          Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
        	  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        	  return earthRadiusKm * c;
        	}
        var service = {
                getDistance : distanceInKmBetweenEarthCoordinates,
                getLocation: getLocation
                
        };
        function getLocation(){
        	return Location;
        }

        return service;
        
    }
})();
