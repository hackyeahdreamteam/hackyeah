package com.rip.hackyeah.repository;

import com.rip.hackyeah.domain.ReportCategory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ReportCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportCategoryRepository extends JpaRepository<ReportCategory, Long> {

}
