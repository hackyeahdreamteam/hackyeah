package com.rip.hackyeah.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Location.
 */
@Entity
@Table(name = "location")
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longtitude")
    private Double longitude;

    @OneToMany(mappedBy = "location")
    @JsonIgnore
    private Set<UserSettings> userSettings = new HashSet<>();

    @ManyToOne
    private Report report;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Location latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Location longitude(Double longtitude) {
        this.longitude = longtitude;
        return this;
    }

    public void setLongitude(Double longtitude) {
        this.longitude = longtitude;
    }

    public Set<UserSettings> getUserSettings() {
        return userSettings;
    }

    public Location userSettings(Set<UserSettings> userSettings) {
        this.userSettings = userSettings;
        return this;
    }

    public Location addUserSettings(UserSettings userSettings) {
        this.userSettings.add(userSettings);
        userSettings.setLocation(this);
        return this;
    }

    public Location removeUserSettings(UserSettings userSettings) {
        this.userSettings.remove(userSettings);
        userSettings.setLocation(null);
        return this;
    }

    public void setUserSettings(Set<UserSettings> userSettings) {
        this.userSettings = userSettings;
    }

    public Report getReport() {
        return report;
    }

    public Location report(Report report) {
        this.report = report;
        return this;
    }

    public void setReport(Report report) {
        this.report = report;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        if (location.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), location.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Location{" +
            "id=" + getId() +
            ", latitude='" + getLatitude() + "'" +
            ", longtitude='" + getLongitude() + "'" +
            "}";
    }
}
