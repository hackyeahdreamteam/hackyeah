package com.rip.hackyeah.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import com.rip.hackyeah.domain.enumeration.AttachmentType;

/**
 * A Attachment.
 */
@Entity
@Table(name = "attachment")
public class Attachment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "path")
    private String path;

    @Column(name = "file_size")
    private Double fileSize;

    @Column(name = "format")
    private String format;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private AttachmentType type;

    @ManyToOne
    private Report report;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Attachment name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public Attachment path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Double getFileSize() {
        return fileSize;
    }

    public Attachment fileSize(Double fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public void setFileSize(Double fileSize) {
        this.fileSize = fileSize;
    }

    public String getFormat() {
        return format;
    }

    public Attachment format(String format) {
        this.format = format;
        return this;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public AttachmentType getType() {
        return type;
    }

    public Attachment type(AttachmentType type) {
        this.type = type;
        return this;
    }

    public void setType(AttachmentType type) {
        this.type = type;
    }

    public Report getReport() {
        return report;
    }

    public Attachment report(Report report) {
        this.report = report;
        return this;
    }

    public void setReport(Report report) {
        this.report = report;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Attachment attachment = (Attachment) o;
        if (attachment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), attachment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Attachment{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", path='" + getPath() + "'" +
            ", fileSize='" + getFileSize() + "'" +
            ", format='" + getFormat() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
