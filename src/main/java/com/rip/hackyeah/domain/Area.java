package com.rip.hackyeah.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Area.
 */
@Entity
@Table(name = "area")
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "radius")
    private Double radius;

    @OneToMany(mappedBy = "area")
    @JsonIgnore
    private Set<Report> reports = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getRadius() {
        return radius;
    }

    public Area radius(Double radius) {
        this.radius = radius;
        return this;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Set<Report> getReports() {
        return reports;
    }

    public Area reports(Set<Report> reports) {
        this.reports = reports;
        return this;
    }

    public Area addReport(Report report) {
        this.reports.add(report);
        report.setArea(this);
        return this;
    }

    public Area removeReport(Report report) {
        this.reports.remove(report);
        report.setArea(null);
        return this;
    }

    public void setReports(Set<Report> reports) {
        this.reports = reports;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Area area = (Area) o;
        if (area.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), area.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Area{" +
            "id=" + getId() +
            ", radius='" + getRadius() + "'" +
            "}";
    }
}
