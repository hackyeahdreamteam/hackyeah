package com.rip.hackyeah.domain.enumeration;

/**
 * The AttachmentType enumeration.
 */
public enum AttachmentType {
    PHOTO, DOCUMENT, ARCHIVE
}
