package com.rip.hackyeah.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ReportCategory.
 */
@Entity
@Table(name = "report_category")
public class ReportCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    private Set<Report> reports = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ReportCategory name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Report> getReports() {
        return reports;
    }

    public ReportCategory reports(Set<Report> reports) {
        this.reports = reports;
        return this;
    }

    public ReportCategory addReport(Report report) {
        this.reports.add(report);
        report.setCategory(this);
        return this;
    }

    public ReportCategory removeReport(Report report) {
        this.reports.remove(report);
        report.setCategory(null);
        return this;
    }

    public void setReports(Set<Report> reports) {
        this.reports = reports;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReportCategory reportCategory = (ReportCategory) o;
        if (reportCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reportCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReportCategory{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
