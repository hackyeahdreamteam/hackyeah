/**
 * View Models used by Spring MVC REST controllers.
 */
package com.rip.hackyeah.web.rest.vm;
