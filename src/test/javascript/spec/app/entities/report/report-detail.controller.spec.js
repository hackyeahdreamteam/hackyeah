'use strict';

describe('Controller Tests', function() {

    describe('Report Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockReport, MockUser, MockReportCategory, MockArea, MockSeverity;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockReport = jasmine.createSpy('MockReport');
            MockUser = jasmine.createSpy('MockUser');
            MockReportCategory = jasmine.createSpy('MockReportCategory');
            MockArea = jasmine.createSpy('MockArea');
            MockSeverity = jasmine.createSpy('MockSeverity');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Report': MockReport,
                'User': MockUser,
                'ReportCategory': MockReportCategory,
                'Area': MockArea,
                'Severity': MockSeverity
            };
            createController = function() {
                $injector.get('$controller')("ReportDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'hackYeahApp:reportUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
